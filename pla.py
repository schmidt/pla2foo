import numpy as np
from enum import IntEnum

class Sym(IntEnum):
    zero = 0    # log 0
    one  = 1    # log 1
    dc   = 2    # don't care
    nm   = 3    # no meaning
    
    def neg (self):
        match(self):
            case Sym.zero: return Sym.one
            case Sym.one:  return Sym.zero
            case _:        return self
    
    @classmethod
    def from_char(cls, char):
        """ input symbols encoding, all variants from espresso(5) """
        match(char):
            case '0':       return cls.zero
            case '1' | '4': return cls.one 
            case '-' | '2': return cls.dc 
            case '~' | '3': return cls.nm 

    def to_char (self):
        """ returns canonical representation """
        match(self):
            case Sym.zero: return '0'
            case Sym.one:  return '1'
            case Sym.dc:   return '-'
            case Sym.nm:   return '~'
            
    @staticmethod
    def canon (n):
        """ return canonical representation of a Sym 
        that has the same number as the int"""     
        match(n):
            case 0: return '0'
            case 1: return '1'
            case 2: return '-'
            case 3: return '~'
           
            
    def compat (self, other):
        """ tests whether symbols are compatible """
        if self is other:             return True
        if self in (Sym.dc, Sym.nm):  return True
        if other in (Sym.dc, Sym.nm): return True
        return False

# ----------------- PLA format complaint ---------------
class PLAError(Exception):
    def __init__(self, reason, file_name, file_lineno, *args):
        self.reason = reason
        self.file_name = file_name
        self.file_lineno = file_lineno
        self.args = args
        
    def str(self):
        res = "PLA format error, file '{}', line {}: ".format(self.file_name, self.file_lineno)
        res += self.reason.format(*self.args)
        return res
# ----------------- PLARepr ----------------------------
class PLARepr:
    """PLA representation"""
    
    types =  ["f", "r", "fd", "fr", "dr", "fdr"]
    
    # compatible Syms - indexed by Sym values
#    compat = np.array([[True,False,True],[False,True,True],[True,True,True]]) 
    
    # what value the output has when no term applies: Sym.nm indicates error
    default = {"f":   Sym.zero, 
               "r":   Sym.one, 
               "fd":  Sym.zero, 
               "fr":  Sym.dc, 
               "dr":  Sym.one, 
               "fdr": Sym.nm}
    
    # what an empty term produces when OR symbol is present
    active = { "f":   [default["f"],           Sym.neg(default["f"])  ],
               "r":   [Sym.neg(default["r"]),  default["r"]           ],
               "fd":  [default["fd"],          Sym.neg(default["fd"]) ],
               "fr":  [Sym.zero,               Sym.one                ],
               "dr":  [Sym.neg(default["dr"]), default["dr"]          ],
               "fdr": [Sym.nm, Sym.nm                                ]}
               
    encode = {'0': Sym.zero,                # input symbols encoding, all vatiants from espresso(5)
              '1': Sym.one, '4': Sym.one, 
              '-': Sym.dc, '2': Sym.dc, 
              '~': Sym.nm, '3': Sym.nm} 
              
    outtr  = {"f":    [Sym.nm,   Sym.one, Sym.nm, Sym.nm],   # real meaning by PLA type
              "r" :   [Sym.zero, Sym.nm,  Sym.nm, Sym.nm], 
              "fd" :  [Sym.nm,   Sym.one, Sym.dc, Sym.nm], 
              "fr" :  [Sym.zero, Sym.one, Sym.nm, Sym.nm], 
              "dr" :  [Sym.zero, Sym.nm,  Sym.dc, Sym.nm], 
              "fdr" : [Sym.zero, Sym.one, Sym.dc, Sym.nm]}
    
    # translate types when removing f, d, r: None indicates error
    dropd = {"f":"f",  "r":"r",  "fd":"f",  "fr":"fr", "dr":"r",  "fdr":"fr"}
    dropr = {"f":"f",  "r":None, "fd":"fd", "fr":"f",  "dr":None, "fdr":"fd"}
    dropf = {"f":None, "r":"r",  "fd":None, "fr":"r",  "dr":"dr", "fdr":"dr"}

    def __init__(self, nin=0, nout=0, nterm=0):
        """Initialize input and output matrixes to zero value;
           shapes are given by the function;
           if nterm == 1 at the time of construction, append() can be used later"""
        self.ilb = None         # input labels
        self.olb = None         # output labels
        self.inmtx =  np.zeros((nterm, nin), np.int8)  
        self.outmtx = np.zeros((nterm, nout), np.int8)
        self.rtype = "fd"
        self.inv = None         # inmtx inversion; where and how to invelidate?
        
    def append(self, rows=1):
        """Extend both matrices by a given number of rows"""
        (inr, inc)   = self.inmtx.shape
        (outr, outc) = self.outmtx.shape
        self.inmtx  = np.append (self.inmtx, np.zeros((rows, inc), np.int8), axis=0)
        self.outmtx = np.append (self.outmtx, np.zeros((rows, outc), np.int8), axis=0)
        
    def setrows(self, rows):
        """Maintain enough space for a new term"""
        (inr, inc)   = self.inmtx.shape
        if inr < rows:
            self.inmtx = np.append (self.inmtx, np.zeros((rows-inr, inc), np.int8), axis=0)
        (outr, outc) = self.outmtx.shape
        if outr < rows:
            self.outmtx = np.append (self.outmtx, np.zeros((rows-outr, outc), np.int8), axis=0)
            
    def terms(self):
        """Returns the number of terms in the PLA"""
        (inr, inc)   = self.inmtx.shape
        return inr
        
    def inputs(self):
        """Returns the number of inputs of the PLA"""
        (inr, inc)   = self.inmtx.shape
        return inc
        
    def outputs(self):
        """Returns the number of outputs of the PLA"""
        (outr, outc) = self.outmtx.shape
        return outc
        
    def literals(self):
        lit=0
        for s in self.inmtx.flatten():
            if s == Sym.zero or s == Sym.one:
                lit += 1
        return lit
        
    def drop_d(self):                    # inefficient - ufunc instead, too simple - rows of "no meaning" shoud be deleted
        self.rtype = PLARepr.dropd[self.rtype]      
        (outr, outc) = self.outmtx.shape
        for i in range (outr):
            for j in range (outc):
                if  self.outmtx[i,j] == Sym.dc:
                    self.outmtx[i,j] =  Sym.nm
        return True

    def drop_r(self):                    # inefficient - ufunc instead, too simple - rows of "no meaning" shoud be deleted
        self.rtype = PLARepr.dropr[self.rtype] 
        if self.rtype == None:           # cannot drop r where it is not
            return False
        (outr, outc) = self.outmtx.shape
        for i in range (outr):
            for j in range (outc):
                if  self.outmtx[i,j] == Sym.zero:
                    self.outmtx[i,j] =  Sym.nm
        return True

    def drop_f(self):                    # inefficient - ufunc instead, too simple - rows of "no meaning" shoud be deleted
        self.rtype = PLARepr.dropf[self.rtype] 
        if self.rtype == None:           # cannot drop f where it is not
            return False
        (outr, outc) = self.outmtx.shape
        for i in range (outr):
            for j in range (outc):
                if  self.outmtx[i,j] == Sym.one:
                    self.outmtx[i,j] =  Sym.nm
        return True
                    
    def simulate(self, test):
        (tlen,tcols) = test.shape  
        assert tcols == self.inputs()
        nout  = self.outputs()
        nin   = self.inputs()
        nterm = self.terms()
        resp = np.zeros((tlen,nout), np.int8)
        resp = np.full_like (resp, Sym.nm) # invalid response code - may not happen
        for t in range(tlen):
            # print(vec)
            sat = 0                     # all outputs undetermined
            nsat = 0
            for irow in range(nterm):
                ncomp = 0;
                for u,v in zip (self.inmtx[irow], test[t]):
                    if Sym(u).compat(Sym(v)):
                        ncomp+=1
                if ncomp < nin:
                    continue            # input vector incompatible
                for outcol in range(nout):
                    if self.outmtx[irow, outcol] != Sym.nm:      # no 'no statement'
                        if resp[t,outcol] == Sym.nm:             # newly set - we take the first valid answer for an output
                            resp[t, outcol] = self.outmtx[irow, outcol]
                            nsat += 1;
                            if nsat == nout:
                                break;
                                
        # any unsatisfied values should go to default by .type
        for t in range(tlen):
            for outcol in range(nout):
                if resp[t,outcol] == Sym.nm: 
                    resp[t,outcol] = PLARepr.default[self.rtype]
                    assert resp[t,outcol] != Sym.nm
        return resp

    def tofile(self, out):
        print (".i", self.inmtx.shape[1], file=out)
        print (".o", self.outmtx.shape[1], file=out)
        print (".p", self.inmtx.shape[0], file=out)
        print (".type", self.rtype, file=out)
        if not (self.ilb is None):
            print (".ilb", ' '.join(name for name in self.ilb), file=out)
        if not (self.olb is None):
            print (".ob", ' '.join(name for name in self.olb), file=out)
        for invec, outvec in zip(self.inmtx, self.outmtx):
            print (''.join(Sym(sym).to_char() for sym in invec), 
                   ''.join(Sym(sym).to_char() for sym in outvec), file=out)
        print (".e", file=out)

    def asblif(self, out):
        assert self.outputs() == 1
        assert not (self.olb is None)       # if .ilb empty, a constant generator
        print (".names",                    # a constant generator cannot have inputs
            ' '.join(name for name in self.ilb) if self.ilb is not None and self.inputs() > 0 else '', 
            ' '.join(name for name in self.olb), 
            file=out)
        for invec, outvec in zip(self.inmtx, self.outmtx):
            print (''.join(Sym(sym).to_char() for sym in invec), 
                   ''.join(Sym(sym).to_char() for sym in outvec), file=out)

    def invert(self):
        self.inv = dict()
        for i in range(self.terms()):
            self.inv[tuple(self.inmtx[i])] = i

    def find(self, term):
        "find a term that exactly occurs in this PLA"
        if self.inv is None:
            self.invert()
        t = tuple(term)
        if t in self.inv:
            return self.inv[t]
        return None
        
    def readfile(self, f):
        self.ilb = None
        self.olb = None
        self.inmtx = None
        self.outmtx = None
        self.rtype = "fd"
        f.lineno = 0          # violating encapsulation
        
        nin = None
        nout = None
        nterm = None
        cterm = 0       # term counter, if no .p line
    
        def encflt(line):
            """filters only input symbols for 0,1,DC,NM - drops space, bars etc."""
            for c in line:
                if c in self.encode:
                    yield c
    
        def dotline(words):
            """ collects data form dot lines into private vars """
            nonlocal nin, nout, nterm, f
            if words[0] in (".i",".o",".p",".ilb",".ob",".type") and len(words) < 2:
                    raise PLAError ('Argument missing', f.name, f.lineno)
            match words[0]:          
                case ".i":    nin = int(words[1])
                case ".o":    nout = int(words[1])
                case ".p":    nterm = int(words[1])
                case ".ilb":  
                    self.ilb = words[1:]
                    if nin is not None and len(self.ilb) != nin:
                        raise PLAError ('Number of input names ({}) does not match .i ({})', 
                            f.name, f.lineno, len(self.ilb), nin)
                case ".ob":   
                    self.olb = words[1:]
                    if nout is not None and len(self.olb) != nout:
                        raise PLAError ('Number of output names ({}) does not match .o ({})', 
                            f.name, f.lineno, len(self.olb), nout)
                case ".e" :   pass         # no terms is legal - a constant generator depending on PLA type
                case ".type": 
                    self.rtype = words[1]
                    if self.rtype not in PLARepr.types:
                        raise PLAError ("PLA type '{}' not supported",
                            f.name, f.lineno, self.rtype)
                case     _:   pass               # MV-related stuff ignored
    
        
        def termline(line):
            """constructs a representation if needed, appends one term"""
            nonlocal nin, nout, cterm
            
            if cterm == 0:
                if nin is None:
                    raise PLAError ('Number of inputs not given', 
                            f.name, f.lineno)               
                if nout is None or nout == 0:
                    raise PLAError ('Number of outputs not given or zero', 
                            f.name, f.lineno)               
                
                self.inmtx =  np.zeros((1, nin), np.int8)  
                self.outmtx = np.zeros((1, nout), np.int8)
            
            self.setrows(cterm+1)
    
            clean = list (self.encode[char] for char in encflt(line))
            if len(clean) != nin+nout:
                raise PLAError ('Number of symbols ({}) != #inputs ({}) + #outputs ({})', 
                    f.name, f.lineno, len(clean), nin, nout)
            for i in range(nin):
                self.inmtx[cterm,i] = clean[i]
            for i in range(nout):
                self.outmtx[cterm,i] = self.outtr[self.rtype][clean[i+nin]]
            cterm +=1
            
        for line in f:
            f.lineno += 1
            line = line.strip()
            if line:                                    # skip empty lines
                match line[0]:
                    case '#': pass                      # comments
                    case '.': dotline (line.split())    # .i, .o, ...
                    case   _: termline(line)            # probably a term; checked further

        if self.inmtx is None:                           # nothing initiated yet
            if nin is None:
                raise PLAError ('Number of inputs not given', 
                        f.name, f.lineno)               
            if nout is None or nout == 0:
                raise PLAError ('Number of outputs not given or zero', 
                        f.name, f.lineno)               
            self.ilb = []
            self.inmtx =  np.zeros((0, 0), np.int8)  
            self.outmtx = np.zeros((0, nout), np.int8)
        if nterm is not None and cterm != nterm:
            raise PLAError ('Number of cubes ({}) does not match .p ({})', 
                f.name, f.lineno, cterm, nterm)

    # ------------------ fromfile --------------------------
    @classmethod
    def fromfile(cls,f):
        """" Returns a representation from a .pla file """
        rep = cls()
        rep.readfile(f)
        return rep

