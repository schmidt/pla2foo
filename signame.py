#!/usr/bin/env /usr/bin/python3
import re
import math
# ---------------------------------------------------------------
def places(n):
    """ returns the number of decimal places of a number """
    return math.ceil(math.log10(n+0.01))
    
# ---------------------------------------------------------------
def name (n, pl, form):
    """ returns signal name for the n-th term 
    according to the format and number of places"""
    res=""
    plain=True
    for c in form:
        if plain:
            if c == '%':
                plain = False
            else:
                res+=c
        else:
            if c == '%':
                res+=c
            elif c == 'n':
                res += str(n).zfill(pl)
            plain=True
    return res

# ---------------------------------------------------------------
def pattern (form, pl):
    """ turns the format into a regexp pattern to check
    existing names in the circuit """
    res="^"
    plain=True
    for c in form:
        if plain:
            if c == '%':
                plain = False
            else:
                res+=re.escape(c)
        else:
            if c == '%':
                res+=c
            elif c == ' ':
                res+='\ ?'  # a space at the end of canonical escaped name 
            elif c == 'n':
                res += '[0-9]{'+str(pl)+'}'
            plain=True
    res += '$'
    return res
# ---------------------------------------------------------------
def is_blif_name (name):
    """ check if the name is a BLIF identifier"""
    return re.search ('^[!-~]*$',name)

# ---------------------------------------------------------------
def is_plain_verilog (name):
    """ check if the name is an unescaped Verilog identifier;
    formats can be checked through name(0, 1, form)"""
    return re.search ('^[a-zA-Z_][0-9a-zA-Z_$]*$', name)

# ---------------------------------------------------------------
def is_escaped_verilog (name):
    """ check if the name is an escaped Verilog identifier;
    trailing space is optional"""
    return re.search ('^\\[!-~]*[ ]?$',name)
    
# ---------------------------------------------------------------
def can_escape_verilog (name):
    """ tests whether a stem is OK to form an escaped identifier"""
    return re.search ('^[!-~]*[ ]?$',name)

# ---------------------------------------------------------------
def verilog_stem (name):
    """ returns the stem of the identifier """
    res = name
    res = res.removeprefix('\\')
    res = res.removesuffix(' ')
    return res

# ---------------------------------------------------------------
def escape_verilog (name):
    res = name
    if res[0] != '\\':
        res = '\\'+res
    if res[-1] != ' ':
        res += ' '
    return res
    
# ---------------------------------------------------------------
def canonicize_name_verilog (name):
    if is_plain_verilog(name):
        return name
    if is_escaped_verilog (name):
        return escape_verilog(name)
    if can_escape_verilog(name):
        return escape_verilog(name)
    return None
# ---------------------------------------------------------------
# assume the format itself is not escaped
def canonicize_format_verilog (form):
    example = name(0,1,form)      ## this is ok <=> all names ok
    if is_plain_verilog(example):
        return form
#    if is_escaped_verilog (example):
#        return escape_verilog(form)
#    stem = verilog_stem(example)
#    if can_escape_verilog (stem):
    if can_escape_verilog(example):
        return escape_verilog(form)
    return None

if __name__ == '__main__':
    import sys
    form = sys.argv[1]
    num  = int(sys.argv[2])

    can_form = canonicize_format_verilog (form)
    print ('Canonical:',can_form)
    pl = places(num)
    print ('Places:',pl)
    print ('Name:',name(num, pl, can_form)+'.')

    patt = pattern(can_form,pl)
    print ('Pattern:', patt)

    sig = 'abc777de'
    print (sig, re.search(patt, sig))

    sig = 'abc77de'
    print (sig, re.search(patt, sig))

    sig = '\\abc[777]de '
    print (sig, re.search(patt, sig))






