# Working with PLA files in synthesis
This document discusses various ways to input PLA files into synthesis systems
and the role of the presented programs.

## TL;DR
If your PLA file does not have shared terms (e.g. produced by `Espresso -Dso`), use ABC:

    > abc
    abc01> read_pla mycirc.pla
    abc02> write_blif mycirc.blif
    abc02> write_verilog mycirc.v

If there are shared terms, and preserving the two-level structure of the PLA file is not need, use:

    > pla2blif -B mycirc.pla mycirc.blif
    > abc
    abc01> read_blif mycirc.blif
    abc02> write_verilog mycirc.v

## Running example
We will use the example `exe.pla` from the [PLA document](/doc/PLA.md/).

    .i 3
    .o 3
    .ilb a b c
    .ob x y z
    .p 7
    -00 1~~
    001 11~
    111 11~
    0-0 1~~
    1-0 ~1~
    -1- ~1~
    1-- ~~1
    .e

## Input to ABC
The ABC command `read_pla` inputs a PLA file to the ABC circuit model. It is immediately
converted to a structure of nodes having a single output each. While the later
steps may recover some of the lost information, the recovery is far from complete.
The command `read_plamo` also suffers from this loss.

To circumvent this deficiency, the BLIF and AIG formats can be used. Internal
data structures for synthesis commands are variants of AIG or XAG. The input
of AIG and the conversion of BLIF use AIG structural hashing, which optimizes
AIG size and depth substantially. Still, the way circuits are input to AIG
should not deny any optimization opportunity.

When reading a PLA file, ABC does not understand the `.type r` specification. 
For such files, it is necessary to use the '-z' option to read the off-set.
This, however, results in negated fuctions for all outputs.

Both the `pla2aag` and `pla2blif` can preserve shared terms as internal signals,
which ABC does not discard. `pla2aag` produces AAG files (AIG textual form)
with no optimization such as literal ordering, which has a slight influence
on the resulting AIG size. `pla2blif` does not prescribe input ordering in
terms, so the results are marginally better.

Using `pla2blif`:

    > pla2blif exe.pla exe.blif
    > abc
    abc01> read_blif exe.blif
    abc02> strash
    ...
    
Using `pla2aag` (the `aigtoaig` program is a part of the AIGER toolset):

    > pla2aag exe.pla exe.aag
    > aigtoaig exe.aag exe.aig
    > abc
    abc01> read_aiger exe.aig
    ...
    
## Converting to BLIF
BLIF is a format introduced by the Berkeley SIS system. The circuit is a network of nodes,
each node has exactly one output (unlike the PLA format) and is described by a table
similar to the PLA format.

### The common (slicing) method using ABC
The standard conversion method using ABC is the following:

    > abc
    abc01> read_pla exe.pla
    abc02> write_blif exe.blif
    abc02> quit
    
Every output has a separate node for its logic function. The resulting file is

    .model exe
    .inputs a b c
    .outputs x y z
    .names a b c x
    -00 1
    001 1
    111 1
    0-0 1
    .names a b c y
    001 1
    1-0 1
    -1- 1
    .names a b c z
    1-- 1
    .end
    
Node tables are optimized by subsumption and cube join (terms differing 
in a single literal are joined to form a larger cube).
The separation of outputs may influence negatively on the quality
of further synthesis. 

### The common (slicing) method using `pla2blif`
A BLIF file with the above structure can also be produced by the 
following command:

    > pla2blif -C exe.pla exe.blif

`pla2blif` lacks the optimization by subsumption and cube join. 
The `-C` option uses the slicing method and is introduced for
comparison only.

### The textbook method using `pla2blif`
A BLIF file can be produced by any of the following commands:

    > pla2blif -T exe.pla exe.blif
    > pla2blif -B exe.pla exe.blif
    
`pla2blif` lacks the optimization by subsumption and cube join. 
The `-B` option preserves only the shared terms as internal signals.
This is the default method, so `-B` can be omitted.
The result is shown below.

The `-T` option introduces all terms as internal signals for simplicity,
which results in larger files.

    .model exe
    .inputs a b c
    .outputs x y z
    .names a b c [t1]
    001 1
    .names a b c [t2]
    111 1
    .names a b c [t1] [t2] x
    -00-- 1
    0-0-- 1
    ---1- 1
    ----1 1
    .names a b c [t1] [t2] y
    1-0-- 1
    -1--- 1
    ---1- 1
    ----1 1
    .names a z
    1 1
    .end

Alternatively, the input to ABC can be produced using `pla2aag` and `aigtoaig`.

## Converting to Verilog or too many ways to skin a PLA file
Verilog `assign` commands are based on expressions. The results 
of all synthesis heuristics depend on the size of the expressions.
Therefore, already any conversion to Verilog should construct
a multilevel structure.

ABC derives Verilog commands from a structurally hashed AIG,
which appears to be an efficient procedure

### The common (slicing) method using ABC
The following commands

    > abc
    abc01> read_pla exe.pla
    abc02> write_verilog exe.v
    
produce the following file:

    // Benchmark "exe" written by ABC on Mon Sep 30 15:48:32 2024
    module exe ( 
        a, b, c,
        x, y, z  );
      input  a, b, c;
      output x, y, z;
      assign x = (~a & (~c | (~b & c))) | (~b & ~c) | (a & b & c);
      assign y = b | (a & ~c) | (~a & ~b & c);
      assign z = a;
    endmodule

### The common (slicing) method using `pla2verilog`
The following command preserves the two-level structure:

    > pla2verilog -C exe.pla exe.v
    
and produces:
    module exe(a,b,c,x,y,z);
    input a,b,c;
    output x,y,z;
    assign x = ((~b & ~c) | (~a & ~b & c) | (a & b & c) | (~a & ~c));
    assign y = ((~a & ~b & c) | (a & b & c) | (a & ~c) | b);
    assign z = a;
    endmodule
    
`pla2verilog` uses decomposition heuristic, so that

    > pla2verilog -A exe.pla exe.v
    
produces:

    module exe(a,b,c,x,y,z);
    input a,b,c;
    output x,y,z;
    assign x = (c ? ((~a & ~b) | (a & b)) : (~b | ~a));
    assign y = ((a ? ((b & c) | ~c) : (~b & c)) | b);
    assign z = a;
    endmodule

### The textbook method using `pla2verilog.py`
To preserve shared terms, use

    > pla2verilog -T exe.pla exe.v
    
and obtain

    module exe(a,b,c,x,y,z);
    input a,b,c;
    output x,y,z;
    wire t1, t2;
    assign t1 = (~a & ~b & c);
    assign t2 = (a & b & c);
    assign x = (((~b & ~c) | (~a & ~c)) | (t1 | t2));
    assign y = (((a & ~c) | b) | (t1 | t2));
    assign z = a;
    endmodule
    
Notice the internal signals `t1` and `t2`. The expressions can be optimized
(this is the default method, the `-B` option can be omitted), so that

    > pla2verilog exe.pla exe.v
    
produces:

    module exe(a,b,c,x,y,z);
    input a,b,c;
    output x,y,z;
    wire t1, t2;
    assign t1 = (~a & ~b & c);
    assign t2 = (a & b & c);
    assign x = (((~b & ~c) | (~a & ~c)) | (t1 | t2));
    assign y = (((a & ~c) | b) | (t1 | t2));
    assign z = a;
    endmodule

### The textbook method using `pla2blif.py` and ABC
Probably the method producing the best result is

    > pla2blif -f 't%n' exe.pla exe.blif
    > abc
    abc01> read_blif exe.blif
    abc02> write_verilog exe.v
    
which gives:

    // Benchmark "exe" written by ABC on Sat Oct  5 22:38:38 2024
    module exe ( 
        a, b, c,
        x, y, z  );
      input  a, b, c;
      output x, y, z;
      wire new_t1, new_t2;
      assign new_t1 = c & ~a & ~b;
      assign new_t2 = c & a & b;
      assign x = (~c & (~a | ~b)) | new_t1 | new_t2;
      assign y = new_t1 | new_t2 | b | (a & ~c);
      assign z = a;
    endmodule
    
The `-f` option sets the format for internal signal names to conform
the unescaped Verilog name format. Even in such a small example,
the expressions are noticeably smaller.
