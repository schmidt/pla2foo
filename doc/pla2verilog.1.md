' # "PLA2VERILOG"("1")
l

## "NAME"

pla2verilog - PLA to Verilog structure-preserving converter 

## "SYNOPSIS"

\w'**pla2verilog.py** 'u**pla2verilog.py** [_options_] [_input-file_ [_output-file_]] 

## "DESCRIPTION"



pla2verilog transforms a combinational circuit specified in the PLA format into a circuit descriptiion in the Verilog format. There are four operational modes to do that. The Common (slicing) mode produces a Verilog assignment for each primary output, which implies that term sharing between output is lost. The Advanced Common mode tries to do multi-level decomposition. That way, it mimics the command write\_verilog in the ABC system, which puts the circuit in an AIG first and can produce better results. The Textbook mode introduces an internal signal for each term shared by more outputs. This way, the sharing is preserved, as been found beneficial in further synthesis. No optimization is attempted. The Advanced Textbook mode preserves sharing similarly to the Textbook mode, yet it tries to decompose the signal specifications like in the Advanced Common Mode, to provide more concise description. The Verilog module name in the produced file is the input ffile name without suffixes, or stdin. If the names of primary inputs and outputs are not given in the input file, they are generated in the form pi_xxx_ or po_xxx_, respectively, where form pi_xxx_ is a constant number of decimal digits. 

## "OPTIONS"



**-l **_log-file_ 

    Logfile name mostly for debugging purposes. Currently no useful information. 

**-t **f | r | auto 

    For PLA files of the fr and fdr types, the conversion can be based either on the given on-set (f), or on the off-set (r). With auto, a heuristic selects which method uses less terms. For fr-type PLA files, both ways are safe with respect to systems such as ABC. For fdr-type PLA files, dont\\*(Aqcares are converted to 0 for the f choice, and to 1 for the r choice, which can cause incompatibility. For example, the ABC reader assumes the f choice. Default is f. 

**-C ** 

    Operate in the Common mode (see DESCRIPTION). The last specified mode applies. 

**-A ** 

    Operate in the Advanced Common mode (see DESCRIPTION). The last specified mode applies. Currently, no XOR decomposition is performed. 

**-T ** 

    Operate in the Textbook mode (see DESCRIPTION). The last specified mode applies. 

**-B ** 

    Operate in the Advanced Textbook mode (see DESCRIPTION). This is the default mode. The last specified mode applies. 

**-f **_ident_ 

    For Textbook mode and Advnced Textbook mode, internal signals must be generated. To avoid name clashes, a signal name format can be provided. The substring %n in the format is replaced by the number of the generated signal. A Verilog name must start with a letter or an underscore, and the following characters must be letters, underscores, digits, or the dollar sign. If this does not hold, the name is escaped, that is, a backslash is prepended and a space is appended. This is performed automatically, which means that a leading backslash is kept and precedded by another backslash during escaping. Use of characters that cannot appear even in escaped name is reported. A possible clash with existing or generated input and output names is reported. 

_input-file_ 

    Input circuit in PLA format. See SEE ALSO. Default input is the standard input. 

_output-file_ 

    output circuit in Verilog format. See SEE ALSO. Default output is the standard output. 

## "SEE ALSO"



PLA format specification 

    espresso(5)\m[blue]**https://people.eecs.berkeley.edu/~alanmi/research/espresso/espresso\_5.html**\m[]\u[1]\d 

Verilog specification 

    Verilog\m[blue]**https://people.cs.georgetown.edu/~squier/Teaching/HardwareFundamentals/LC3-trunk/docs/verilog/VerilogLangRef.pdf**\m[]\u[2]\d 

The ABC system 

    Mishchenko, A., et al.: ABC: A system for sequential synthesis and verification,(2012), \m[blue]**http://www.eecs.berkeley.edu/˜alanmi/abc**\m[] 

## "BUGS"



The decomposition method is not efficient enough and can produce files too large for synthesis systems. Formatting of the output file is far from readable or pretty. 

## "BUG REPORT"



Please send bug report to \<jan.schmidt@fit.cvut.cz> 

## "AUTHOR"



Jan Schmidt, Dept. of Digital design, Fac. of Information Technology, Czech Technical University in Prague. `man2md` converter by phillbush, https://github.com/phillbush/man2md 

## "NOTES"



https://people.eecs.berkeley.edu/~alanmi/research/espresso/espresso\_5.html 

    \%https://people.eecs.berkeley.edu/~alanmi/research/espresso/espresso\_5.html 

https://people.cs.georgetown.edu/~squier/Teaching/HardwareFundamentals/LC3-trunk/docs/verilog/VerilogLangRef.pdf 

    \%https://people.cs.georgetown.edu/~squier/Teaching/HardwareFundamentals/LC3-trunk/docs/verilog/VerilogLangRef.pdf 