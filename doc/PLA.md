# The Berkeley PLA files 

## Origin
The PLA format was introduced in eighties for design systems developed in Berkeley.
It was closely related to contemporary implementation of combinational logic
as Programmable Logic Arrays. An input matrix ("AND plane") routed input signal
to AND arrays, producing product terms (cubes). These were directed by another
matrix ("OR plane") to OR gates, which produced output signals.

The size of the implementation was proportional to the number of terms,
and therefore design tools (notably `espresso` minimizer) minimized the
_total_ number of terms, sharing some terms between outputs whenever 
possible.

## Example
Let us have the following PLA file. It has three inputs, named `a`,`b`, and `c`,
and three inputs, named `x`,`y`, and `z`. There are seven product terms.


    .i 3
    .o 3
    .ilb a b c
    .ob x y z
    .p 7
    -00 1~~
    001 11~
    111 11~
    0-0 1~~
    1-0 ~1~
    -1- ~1~
    1-- ~~1
    .e

The left-hand side of a product specification defines literals in the term.
`0` means a negated variable literal, `1` means a direct variable literal,
`-` means the absence of the particular variable from the term. The right-hand
part specifies how the product participates in a particular output. `1` means
that satisfying the term implies `1` on that output, while `~` has no statement
and is a mere placeholder.

A textbook implementation is in the following figure. 

![example circuit schematic](/doc/exe.svg/)

From the PLA file and from the schematic, we can see that products #1 and #2
are shared between two outputs.

## Conversion to formats with single-output blocks
The PLA format can describe logic as a two-level circuit only. For multilevel
synthesis, other formats must be used. Many of them share a feature - the description
consists of blocks (entities), which have a single output only. For example:

* The BLIF format, used by SIS and ABC, has a similar description of terms, 
however its blocks are limited to a single output each.
* The ABC system transforms a PLA description in the same manner before any use.
* Verilog and other HDLs assigns an expression to a (single) signal.

The standard way is to *duplicate* the terms shared between outputs,
and then for each input, taking only the corresponding
*slice* of the OR-plane. So, we will call this the *slicing method*.

This way,
the information that a term is shared gets lost. Synthesis systems 
are supposed to recover it, however, we discovered cases when they 
fail.

Another way is to follow the above figure, and define the shared
terms as internal signals of a block. This is the *textbook method*,
including certain optimizations.

## On-sets and off-sets
The above example defined a completely specified function by its _on-set_,
that is, by the set of points where the function yields `1`. The other points
were assumed to produce `0`. The other way round, a completely specified function
can be defined by its _off-set_, the set of points where the function produces `0`.
The same function as above can be described as
    .type r
    .i 3
    .o 3
    .ilb a b c
    .ob x y z
    .p 5
    110 0~~
    011 0~~
    101 00~
    000 ~00
    0-- ~~0
    .end
Notice the keyword `.type`, its parameter says that an off-set will follow.
Also, the `0` in the right-hand side of term description says that 
satisfying the term implies a `0` at the particular output, while otherwise
it would be `1`. 

On-set specification is introduced by `.type f`, which is the default,
off-set specification is introduced by `.type r`. There is also `.type fr`,
which indicates that both the on-set and the off-set are present. In that
case, the software processing the file decides which set to use.
The function points omitted by such a specification get a default value,
which is also decided by the software.

A textbook implementation is in the following schematic, notice that it
seems simpler.

![example circuit schematic](/doc/exer.svg/)

## Incompletely specified functions and DC-sets
The functions discussed so far were completely specified, that is, each
function point has a determined value. In the case of *incompletely specified*
functions, there is a set of function points with undefined (dont't care, DC)
values, the DC-set. 

A DC-set can be specified in a PLA file, the types then become
`fd`,`dr`, or `fdr`.

The synthesis formats discussed above do not support incompletely specified
functions. Commonly, when given such a function, the software assigns
all points in the DC-set by a chosen value. For the `fd` type, it is naturally 
`0`. For the `dr` type, it is `1`. For the `fdr` type, it can depend on
the choice of the defining set.

