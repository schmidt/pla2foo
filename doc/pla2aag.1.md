' # "PLA2AAG"("1")
l

## "NAME"

pla2aag - PLA to AAG structure-preserving converter 

## "SYNOPSIS"

\w'**pla2aag.py** 'u**pla2aag.py** [_options_] [_input-file_ [_output-file_]] 

## "DESCRIPTION"



pla2blif transforms a combinational circuit specified in the PLA format into a circuit descriptiion in the AAG format. Unlike common procedures for this purpose, found in many systems, it preserves the structure of the original, that is, it describes the AND-plane and the OR-plane separately, and preserves the signal sharing between outputs. This has been found beneficial in further synthesis. 

For PLA files of the fr and fdr types, such a conversion can be based either on the given on-set, or on the off-set of the described function. For fr-type PLA files, both ways are safe with respect to systems such as ABC. For fdr-type PLA files however, dont\\*(Aqcares are converted when based on the on/set, and to 1 when based on the off-set, giving incompatible results. Therefore, pla2aag works with the on-set only the same way as the ABC reader. 

## "OPTIONS"



**-l **_log-file_ 

    Logfile name mostly for debugging purposes. Currently no useful information. 

_input-file_ 

    Input circuit in PLA format. See SEE ALSO. Default input is the standard input. 

_output-file_ 

    output circuit in AAG format. See SEE ALSO. Default output is the standard output. Use aigtiaig from the AIGER tool set to convert to AIG. 

## "SEE ALSO"



PLA format specification 

    espresso(5)\m[blue]**https://people.eecs.berkeley.edu/~alanmi/research/espresso/espresso\_5.html**\m[]\u[1]\d 

AAG format specification and the AIGER tool set 

    Armin Biere et al: AIGER \m[blue]**https://fmv.jku.at/aiger/index.html**\m[] 

## "BUGS"



The AAG file is produced without optimization, using the lexicographical ordering of variables and terms. Also, the PLA specification is not optimized by, e.g., term subsumption and cube join. 

## "BUG REPORT"



Please send bug report to \<jan.schmidt@fit.cvut.cz> 

## "AUTHOR"



Jan Schmidt, Dept. of Digital Design, Fac. of Information Technology, Czech Technical University in Prague. `man2md` converter by phillbush, https://github.com/phillbush/man2md 

## "NOTES"



https://people.eecs.berkeley.edu/~alanmi/research/espresso/espresso\_5.html 

    \%https://people.eecs.berkeley.edu/~alanmi/research/espresso/espresso\_5.html 