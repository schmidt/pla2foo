' # "PLA2BLIF"("1")
l

## "NAME"

pla2blif - PLA to BLIF structure-preserving converter 

## "SYNOPSIS"

\w'**pla2blif.py** 'u**pla2blif.py** [_options_] [_input-file_ [_output-file_]] 

## "DESCRIPTION"



pla2blif transforms a combinational circuit specified in the PLA format into a circuit descriptiion in the BLIF format. There are three operational modes to do that. The Common (slicing) mode produces a single `.names` node for each primary output, which implies that term sharing between output is lost. The Textbook mode introduces an internal signal for each term shared by more outputs. It describes the AND-plane and the OR-plane by a structure of AND and OR gates. This way, the sharing is preserved, as it has been found beneficial in further synthesis. No optimization is attempted. The Advanced Textbook mode preserves sharing similarly to the Textbook mode, yet only the shared terms are defined as internal signals. more concise description. 

## "OPTIONS"



**-l **_log-file_ 

    Logfile name mostly for debugging purposes. Currently no useful information. 

**-t **f | r | auto 

    For PLA files of the fr and fdr types, the conversion can be based either on the given on-set (f), or on the off-set (r). With auto, a heuristic selects which method uses less terms. For fr-type PLA files, both ways are safe with respect to systems such as ABC. For fdr-type PLA files, dont\\*(Aqcares are converted to 0 for the f choice, and to 1 for the r choice, which can cause incompatibility. For example, the ABC reader assumes the f choice. Default is f. 

**-C ** 

    Operate in the Common mode (see DESCRIPTION). The last specified mode applies. 

**-T ** 

    Operate in the Textbook mode (see DESCRIPTION). The last specified mode applies. Default mode. 

**-B ** 

    Operate in the Advanced Textbook mode (see DESCRIPTION). This is the default mode. The last specified mode applies. 

_input-file_ 

    Input circuit in PLA format. See SEE ALSO. Default input is the standard input. 

_output-file_ 

    output circuit in BLIF format. See SEE ALSO. Default output is the standard output. 

## "SEE ALSO"



PLA format specification 

    espresso(5)\m[blue]**https://people.eecs.berkeley.edu/~alanmi/research/espresso/espresso\_5.html**\m[]\u[1]\d 

BLIF format specification 

    University of California, Berkeley: Berkeley Logic Interchange Format ( BLIF) \m[blue]**https://people.eecs.berkeley.edu/~alanmi/publications/other/blif.pdf**\m[]\u[2]\d 

## "BUGS"



The AAG file is produced without optimization, using the lexicographical ordering of variables and terms. Also, the PLA specification is not optimized by, e.g., term subsumption and cube join. 

## "BUG REPORT"



Please send bug report to \<jan.schmidt@fit.cvut.cz> 

## "AUTHOR"



Jan Schmidt, Dept. of Digital design, Fac. of Information Technology, Czech Technical University in Prague. `man2md` converter by phillbush, https://github.com/phillbush/man2md 

## "NOTES"



https://people.eecs.berkeley.edu/~alanmi/research/espresso/espresso\_5.html 

    \%https://people.eecs.berkeley.edu/~alanmi/research/espresso/espresso\_5.html 

https://people.eecs.berkeley.edu/~alanmi/publications/other/blif.pdf 

    \%https://people.eecs.berkeley.edu/~alanmi/publications/other/blif.pdf 