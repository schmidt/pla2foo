# PLA2AAG AND PLA2BLIF

## Purpose
Common Electronic Design Automation tools, such as SIS or ABC, work on
networks of single-output logic nodes (in the case of ABC, mostly 
transforming it into other representations). 
Such a structure is also represented by the BLIF format. 

In some situation, the design has to start with a two-level specification,
usually represented in the [PLA](/doc/PLA.md/) format.
Group minimization then minimiyes the number of terms and
maximizes the amount of logic shared by
different output signals. The resulting data are again represented
in the PLA format.

The conversion of such a structure into single-output nodes commonly proceeds
output by output. The truth table is reduced (sliced) for the chosen
output, and only necessary terms are kept. This way, the information about
logic sharing is lost, with the hope that the following optimization steps
will be able to recover it. This is not always the case, especially when
the logic functions are only sparsely specified. This is what we call
the Common or Slicing Method.

Therefore, `pla2aag`, `pla2blif` and `pla2verilog`  perform the 
conversion differently. They describe the network as a 'textbook' 
structure of gates. Therefore, we refer to this method as the 
Textbook Method. The AND plane is generated first, producing 
a set of internal signals, one for each  term. 
The OR plane gates are generated next, using the (shared) signals 
to generate output signals. 

For a more thorough discussion of working with PLA files, please
refer to the [USAGE](/doc/USAGE.md/) document.

## Installation
The programs are Python scripts, tested with Python 3.11. `pla2aag`,
`pla2blif` and `pla2verilog` are meant to be executable, 
while the other `.py` files
are utility modules imported into the executable programs.

The simplest way to install is to copy `pla2aag`,
`pla2blif` and `pla2verilog` and all `.py` files into a directory
included in the executable path list. Otherwise, the module files can be
also placed in a directory where Python looks for modules.

On Unix systems, the Makefile can do that for you. Available targets are:

    install
    install-man
    doc
    
The `install` target installs programs and modules. 
It requires the value of `BINDIR` (the executables
directory), and `PYTHONDIR` (the modules directory), if different.

The `install-man` target installs manual pages. It requires
the value of MANDIR, the manual pages directory that contains
the `man1` and similar subdirectories.

The `doc` target recreates various forms of manual pages
from their XML (docbook) source format. It uses the `xmlto`
utility.

For example:
    make BINDIR=/usr/local/bin PYTHONDIR=/usr/local/lib/python install
    make MANDIR=/usr/local/man install-man

## Usage
The programs can process PLA files of any type (f, fd, fr, fdr, r, dr).
When the PLA is meant to have a dc-set, however, the programs have to
replace don't cares with fixed values, which are 1 for the dr type 
and 0 otherwise.

The commands are

    pla2aag.py  [options] [input-file [output-file]]
    pla2blif.py [options] [input-file [output-file]]
    pla2verilog.py [options] [input-file [output-file]]
    
When not specified, input is the standard input and output is 
the standard output.

For conversion methods, please refer to the [USAGE](/doc/USAGE.md/).
For options, please see the manual pages.

## References

PLA format: espresso(5) 
https://people.eecs.berkeley.edu/~alanmi/research/espresso/espresso_5.html

AIG format specification and the AIGER tool set:
Armin Biere et al: AIGER, https://fmv.jku.at/aiger/index.html

BLIF format specification:
University of California, Berkeley: Berkeley Logic Interchange Format (BLIF)
https://people.eecs.berkeley.edu/~alanmi/publications/other/blif.pdf

ABC system
Mishchenko, A., et al.: ABC: A system for sequential synthesis and verification
(2012), http://www.eecs.berkeley.edu/˜alanmi/abc

SIS system
E. Sentovitch, K. Singh et al.: ”SIS: A System for sequential circuit synthesis”,
Univ. California, Berkeley, Tech. Rep., UCB/ERL M92/41, May 1992


