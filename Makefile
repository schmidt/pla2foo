
PROGS := pla2aag pla2blif pla2verilog
export PROGS MANDIR

MODULES := pla.py aig.py statpla.py signame.py
# if BINDIR is defined, but PYTHONDIR is not:
PYTHONDIR ?= $(BINDIR)

.PHONY: doc

all:
	@echo "Available targets:"
	@echo "doc"
	@echo "install (define BINDIR and, if different, PYTHONDIR)"
	@echo "install-man (define MANDIR)"

install: 
ifeq ($(strip $(BINDIR)),)
	@echo "BINDIR is not defined, use e.g., 'make BINDIR=/usr/local/bin'"
else
	mkdir -p $(BINDIR)
	cp $(PROGS) $(BINDIR)
	mkdir -p $(PYTHONDIR)
	cp $(MODULES) $(PYTHONDIR)
endif

doc:
	$(MAKE) -C doc
	
install-man:
ifeq ($(strip $(MANDIR)),)
	@echo "MANDIR is not defined, use e.g., 'make MANDIR=/usr/local/man install-man'"
else
	$(MAKE) install-man -C doc
endif 
