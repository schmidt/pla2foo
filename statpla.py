import pla
import math
import re
import logging
import pathlib
import signame

# ---------------------------------------------------------------
# PLA representation with transformations to BLIF in mind
#   two mode transformations:
#   fmode: fd -> f, fr -> f, fdr -> f with DC -> 0
#   rmode: rd -> r, fr -> r, fdr -> r with DC -> 1
# ---------------------------------------------------------------
class PLARepr(pla.PLARepr):
    """ a PLA representation with added functions
        for BLIF conversion """
    def __init__(self, nin, nout, nterm):
        super().__init__(nin, nout, nterm)
    
    needed_or = [pla.Sym.zero, pla.Sym.one]     # True -> mode f, false -> mode r

    def andPlaneNodes(self):
        """ estimates the number of AND nodes in AAG 
            to implement the AND plane of the PLA """
        cnt = 0     # total count of nodes to implement the AND plane
        for t in range (self.terms()):
            if not self.needed[t]: continue
            tcnt = 0
            for sym in self.inmtx[t]:
                if sym in (pla.Sym.zero,pla.Sym.one):
                    tcnt += 1
            match tcnt:
                case 0: pass        # unspecified term
                case 1: tcnt = 0    # one literal needs no AND
                case _: tcnt-=1      # a tree with n inputs has n-1 nodes
            cnt += tcnt
        return cnt

    def orPlaneNodes(self, mode):
        """ estimates the number of AND nodes in AAG 
            to implement the OR plane of the PLA """
        cnt = 0     # total count of nodes to implement the AND plane
        for col in range(0, self.outmtx.shape[1]):
            colcnt = 0
            for t in range (self.terms()):
                if not self.needed[t]: continue
                sym = self.outmtx[t,col]
                if sym == self.needed_or[mode]:
                    colcnt += 1
            match colcnt:
                case 0: pass        # no terms to OR
                case 1: colcnt = 0    # one term needs no OR
                case _: colcnt -= 1
            cnt += colcnt
        return cnt
   
    # -----------------------------------------------------------
    def need_term (self, term, mode):
        """ returns whether a given term is needed for output """
        return self.needed_or[mode] in term
        
    def need_terms (self, mode):
        """ returns a vector of bools indicating 
            whether a given term is needed for any output """
        needed = [False] * self.terms()
        for t in range(self.terms()):
            needed[t] = self.need_term (self.outmtx[t], mode)
        return needed

    # -----------------------------------------------------------
    def need_unique (self, term, mode):
        """ returns for how many outputs the term is needed
        and the index of the last one """
        count = 0;
        last = None;
        for i in range(self.outputs()):
            if term[i] == self.needed_or[mode]:
                count += 1
                last = i
        return (count,last)

    # -----------------------------------------------------------
    def need_uniques (self, mode):
        """ returns a list: if a term is uniquely needed, then 
        the output index; if no, None"""
        needed = [None] * self.terms()
        for t in range(self.terms()):
            count,unique = self.need_unique (self.outmtx[t], mode)
            if count == 1:
                needed [t] = unique
        return needed        

    # -----------------------------------------------------------
    def default_in_names(self, form):
        """ sets input names to pi... or the like according to format"""
        ipl = signame.places(self.inputs())
        self.ilb = [signame.name(n, ipl, form) for n in range(self.inputs())]

    def default_out_names(self, form):
        """ sets output names to po... or the like according to format"""
        opl = signame.places(self.outputs())
        self.olb = [signame.name(n, opl, form) for n in range(self.outputs())]

    # -----------------------------------------------------------
    def fr_heuristic(self):
        f_needed = self.need_terms(True)
        r_needed = self.need_terms(False)
        f_num = 0
        for b in f_needed:
            if (b):
                f_num += 1
        r_num = 0
        for b in r_needed:
            if (b):
                r_num += 1
        return f_num < r_num
    
          

