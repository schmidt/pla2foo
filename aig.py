# loosely based on
# John Komp, University of Colarado at Boulder
# https://github.com/JKomp/AIGER

# ==============================================================
class AIGRepr:
    """ simplified AIG representation,
        only inputs, outputs and AND nodes ssupported """
    def __init__ (self,nin, nout, ngates, nvar):
        self.num_inputs  = 0           # current circuit inputs
        self.num_outputs = 0           # current circuit outputs
        self.num_ands    = 0           # current gates
        self.max_var     = nvar        # max existing var index per spec

        self.inputs      = [None] * nin      # [0..num_inputs]
        self.outputs     = [None] * nout     # [0..num_outputs]
        self.ands        = [None] * ngates   # [0..num_ands]
        self.nodes       = [None] * (nvar+1) # [0..max index] 
                                             # nodes[0] is the zero constant
        
        self.nodes[0]    = AIGConst();
        
    def register_input (self, inobj):
        """ adds an input object to the representation """
        self.inputs [self.num_inputs] = inobj   # list of inputs
        self.nodes  [inobj.lit >> 1] = inobj    # list of node objects
        inobj.index = self.num_inputs
        self.num_inputs += 1

    def register_output (self, outobj):
        """ adds an output object to the representation """
        self.outputs[self.num_outputs] = outobj
        self.nodes  [outobj.lit >> 1] = outobj
        outobj.index = self.num_outputs
        self.num_outputs += 1

    def register_and (self, andobj):
        """ adds an and node object to the representation """
        self.ands   [self.num_ands] = andobj
        self.num_ands += 1
        self.nodes  [andobj.lit >> 1] = andobj

    def deal_and_lit (self):
        """ reserves place for one and object in the lists """
        self.ands.append(None)
        self.nodes.append(None)
        self.max_var += 1        
        return self.max_var << 1

    def write(self,outfile):
        """ write the representation to an AAG file """
        print ('aag', 
            self.max_var, 
            self.num_inputs, 
            0, 
            self.num_outputs, 
            self.num_ands, file=outfile)
        for inobj in self.inputs:
            if inobj:
                inobj.write(outfile)
        for outobj in self.outputs:
            if outobj:
                outobj.write(outfile)
        for gate in self.ands:
            if gate:
                gate.write(outfile)
        for inobj in self.inputs:
            if inobj:
                inobj.write_sym(outfile)
        for outobj in self.outputs:
            if outobj:
                outobj.write_sym(outfile)
        
# ==============================================================
class AIGNode:
    def __init__(self, literal):
        self.lit = literal
        assert (literal >= 0)
        
# ==============================================================
class AIGConst(AIGNode):
    def __init__(self):
        super().__init__(0)
       
# ==============================================================
class AIGInput(AIGNode):
    def __init__(self, literal, sym=''):            
        # literal is the true literal of the input
        super().__init__(literal)
        self.index = 0;
        self.symbol = sym
        assert ((literal & 1) == 0)
    def write(self,outfile):
        print (self.lit,  file=outfile)
    def write_sym(self,outfile):
        if self.symbol:
            print ('i'+str(self.index), self.symbol, file=outfile)

# ==============================================================
class AIGOutput(AIGNode):
    def __init__(self, literal, sym=''):            
        # literal is the literal of the gate which feeds the output
        super().__init__(literal)
        self.index = 0;
        self.symbol = sym
    def write(self,outfile):
        print (self.lit,  file=outfile)
    def write_sym(self,outfile):
        if self.symbol:
            print ('o'+str(self.index), self.symbol, file=outfile)

# ==============================================================
class AIGAnd(AIGNode):
    def __init__(self, literal, in0, in1):  # all args are literals
        super().__init__(literal)
        assert ((literal & 1) == 0)
        assert (in0 >= 0 and in1 >= 0)
        self.in0 = in0;
        self.in1 = in1;
    def write(self,outfile):
        print (self.lit, self.in0, self.in1, file=outfile)

# ==============================================================
def from_aag_file (infile):
    hdr = (infile.readline()).split()
    assert (len(hdr) >= 6)
    assert (hdr[0] == 'aag')
    maxv   = int(hdr[1])
    nin    = int(hdr[2])
    nlatch = int(hdr[3])
    nout   = int(hdr[4])
    ngates = int(hdr[5])
    assert (nin > 0)
    assert (nlatch == 0)
    assert (nout > 0)
    assert (ngates > 0)
    assert (maxv >= nin+ngates)
    
    model  = AIGRepr (nin, nout, ngates, maxv)

    for i in range(0,nin):
        line = (infile.readline()).split()
        assert (len(line) == 1)
        model.register_input (AIGInput(int(line[0])))

    for i in range(0,nout):
        line = (infile.readline()).split()
        assert (len(line) == 1)
        model.register_output (AIGOutput(int(line[0])))

    for i in range(0,ngates):
        line = (infile.readline()).split()
        assert (len(line) == 3)
        model.register_and (AIGAnd(int(line[0]), int(line[1]), int(line[2])))

    line = infile.readline()
    while (line):
        line = line.split()
        match (line[0][0]):
            case 'i':
                model.inputs[int(line[0][1:])].symbol = line[1].rstrip()
            case 'l':
                print('latch')
            case 'o':
                model.outputs[int(line[0][1:])].symbol = line[1].rstrip()
            case 'c':
                break           # comment section comming                
        line = infile.readline()
        
    return model
 
    
